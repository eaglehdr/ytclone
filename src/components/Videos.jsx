import React from 'react'
import  VideoCard from './VideoCard'
import ChannelCard from './ChannelCard'


function Videos({videos}) {
  // let tempc = 20;
  console.log(videos)
  return (
    <div className="mt-4  grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
    {videos.map((item,idx) => (
        <div className="empty:hidden" key={`yt-${idx}`}>
            {item.id.videoId && <VideoCard key={item.id.videoId} video={item}/>}
            {item.id.channelId && <ChannelCard key={item.id.channelId} channedlDetails={item}/>}
        </div>
    ))}
    </div>
  )

  
}

export default Videos