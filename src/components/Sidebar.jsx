import React from 'react'

import { categories} from '../utils/constants'

// const selectedCategory = 'New'

function Sidebar({selectedCategory, setSelectedCategory}) {
  return (
    <div className="h-auto md:h-[92vh] border-r-2 border-[#3d3d3d] sm:w-[20%] md:w-[25%] lg:w-[15%] px-2 md:px-2 w-full mt-4">
      <div className="overflow-y-auto flex sm:flex-col">
          {categories.map((category, index) => (
              <button 
                key={`${'yt-btn_'+index}`} 
                className={(`category-btn text-gray-900 dark:text-white rounded-full mb-2 p-1.5 text-left mx-auto flex flex-none sm:w-full sm:flex-1 sm:justify-start max-w-[250px] items-center hover:bg-ytred hover:text-white ${category.name === selectedCategory ? 'bg-ytred text-white' : ''} transition`)}
                onClick={() => { setSelectedCategory(category.name)}}  
              >
                  <div  className="pl-4" >{category.icon}</div >
                  <div  className="px-2">{category.name}</div >
              </button>
          ))}
      </div>
      <div  className="copyright mt-1.5 hidden sm:block dark:text-white text-gray-900">
        Copyright 2022 YTC
      </div>
    </div>
  )
}

export default Sidebar