import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import SearchBar from './SearchBar'
import { Switch } from '@headlessui/react'

import {logo} from '../utils/constants'

const Navbar = () => {

  const [darkEnabled, setDarkEnabled] = useState(false)

  const setDarkMode = (darkEnabled) => {
    if(darkEnabled) {
      document.documentElement.classList.add('dark')
      localStorage.setItem('darkMode', true)
    }
    else {
      document.documentElement.classList.remove('dark')
      localStorage.removeItem('darkMode')
    }
  }
  const toggleDarkMode = (darkEnabled) => {
    setDarkMode(darkEnabled)
    setDarkEnabled(darkEnabled)
    return;
  }

  useEffect(() => {
    if(localStorage.getItem('darkMode')) {
      setDarkEnabled(true)
      setDarkMode(localStorage.getItem('darkMode'))
    }else {
      setDarkEnabled(false)
      setDarkMode()
      
    }
  }, [])

  return (
    <div className="flex justify-between items-center p-2 sticky dark:bg-black bg-white top-0 shadow-lg">
      <div className="flex items-center">
        <Link to="/" className="flex items-center">
          <img src={logo} alt="logo" className="h-12" /> 
        </Link>
        <Switch
          checked={darkEnabled}
          onChange={toggleDarkMode}
          className={`${
            darkEnabled ? 'bg-blue-600' : 'bg-gray-200'
          } relative inline-flex h-6 w-11 items-center rounded-full ml-4`}
        >
          <span className="sr-only">Enable notifications</span>
          <span
            className={`${
              darkEnabled ? 'translate-x-6' : 'translate-x-1'
            } inline-block h-4 w-4 transform rounded-full bg-white transition`}
          />
        </Switch>
      </div>
      <SearchBar/>
    </div>
  )
  
}


export default Navbar