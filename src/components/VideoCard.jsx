import React from 'react'
import {Card} from 'flowbite-react'
import {Link } from 'react-router-dom'
import {demoThumbnailUrl, demoChannelUrl, demoVideoUrl, demoChannelTitle, demoVideoTitle, demoProfilePicture} from '../utils/constants'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheck} from '@fortawesome/free-solid-svg-icons'

// function VideoCard({video}) { we could use this and then use video.id.videoId instead, but we will destructure a littlebit
function VideoCard({video : {id : {videoId}, snippet}}) {
  return (
      <Card key={videoId} id={videoId} className="max-w-sm cursor-pointer" imgAlt={snippet.title} imgSrc={snippet?.thumbnails?.medium?.url}>
        <Link to={videoId ? `video/${videoId}` : demoVideoUrl}>
          <h5 className="text-xl font-bold tracking-tight text-gray-900 dark:text-white line-clamp-2 text-ellipsis" title={snippet?.title}>
          {snippet?.title || demoVideoTitle } 
          </h5>
        </Link>
        <p className="text-base font-normal text-gray-700 dark:text-gray-400">
          <Link to={snippet?.channelId ? `channel/${snippet.channelId}` : demoChannelUrl}>
            {snippet?.channelTitle || snippet?.channelTitle}
          </Link>
        </p>
      </Card>
  )
}

export default VideoCard