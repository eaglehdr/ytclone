import React from 'react'
import { useState } from 'react'
import { userNavigate } from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';


function SearchBar() {
  const [searchText, setSearchText] = useState();
  
  return (
    <form onSubmit={() => {}} className="container rounded-full border border-[#e3e3e3] bg-white   text-black  pl-2 mr-5 sm:mr-0 max-w-sm w-full py-0  flex">
        <input className="search-bar border-0 leading-4 h-auto w-full px-4 py-2 bg-transparent focus:outline-none" placeholder="Search..." value={searchText} onChange={() => { setSearchText(this.value)}} />
        <button type="submit" className=" text-red-600 p-2 w-12 rounded-full" ><FontAwesomeIcon icon={faSearch} /></button>
    </form>
  )
}

export default SearchBar