import React from 'react'
// import {Card} from 'flowbite-react'
import {Link } from 'react-router-dom'
import {demoThumbnailUrl, demoChannelUrl,  demoChannelTitle} from '../utils/constants'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheck} from '@fortawesome/free-solid-svg-icons'

function ChannelCard({channedlDetails, classes}) {
    setTimeout(() => {
      console.log(channedlDetails)
      
    }, 1000);
    // let channelId = channedlDetails?.id?.channelId channedlDetails.id.channelId : channedlDetails?.id;
    // let snippet = channedlDetails?.snippet
    return (
        <div key={channedlDetails?.id?.channelId || channedlDetails?.id } id={channedlDetails?.id?.channelId || channedlDetails?.id} className={`flex rounded-lg  flex-col max-w-sm cursor-pointer border-0 shadow-none h-full justify-center ${classes}`}>
          <Link to={channedlDetails ? `channel/${channedlDetails.id }`: demoChannelUrl}>
            <div className="flex flex-col">
                <img src={channedlDetails?.snippet?.thumbnails?.high?.url || channedlDetails?.snippet?.thumbnails?.default?.url ||  demoThumbnailUrl } className="max-w-[200px] mx-auto mb-2" alt="" />
                <p className="text-xl text-center font-bold text-gray-700 dark:text-gray-400">{channedlDetails?.snippet?.channelTitle || demoChannelTitle}</p>
            </div>
          </Link>
        </div>
    )
}

export default ChannelCard