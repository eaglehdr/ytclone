import React from 'react'
import { useState, useEffect } from 'react'

import Sidebar from './Sidebar'
import Videos from './Videos'
import {fetchFromApi } from '../utils/fetchFromAPI'

const Feed = () => {
  const [selectedCategory, setSelectedCategory] = useState('JS Mastery')
  const [videos, setvideos] = useState([])

  useEffect(() => {
    fetchFromApi(`search?part=snippet&q=${selectedCategory}`)
    .then((data) => {
      setvideos(data.items)
    })
  }, [selectedCategory]);
  
  return (
    <div className="flex flex-col sm:flex-row bg-white dark:bg-black pt-4 ">
      <Sidebar selectedCategory={selectedCategory} setSelectedCategory={setSelectedCategory} />
      <div className="feed-main w-[75%] px-2 overflow-y-auto h-90vh flex-2">
        <h2 className="text-4xl font-bold" >{selectedCategory} Videos</h2>
        <div className="videos-container">
            <Videos videos={videos}/>
        </div>
      </div>
    </div>
  )
}

export default Feed