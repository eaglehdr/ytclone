import React, { useState, useEffect} from 'react'
import { useParams } from 'react-router-dom'

import Videos from './Videos'
import ChannelCard from './ChannelCard'
import { fetchFromApi } from '../utils/fetchFromAPI'

const Channel = () => {
  const [channelDetails, setChannelDetails] = useState(null)
  const [videos , setVideos] = useState([])
  const { id } = useParams()

  useEffect(() => {
    fetchFromApi(`channels?part=snippet&id=${id}`)
      .then((data) => setChannelDetails(data?.items[0]))
      // .then((data) => console.log(data.items[0]))

    fetchFromApi(`search?channelId=${id}&part=snippet&order=date`)
      .then((data) => setVideos(data?.items))    
      // .then((data) => console.log(data))

    }, [id])
    console.log(channelDetails, videos)
  
  return (
    <div className="min-h-[95vh]">
      <div className="bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 z-10 h-[300px]"></div>
      <div className="channel-logo mx-auto text-center flex justify-center">
        { channelDetails  &&   <ChannelCard channelDetails={channelDetails} classes={`-mt-12`}/>  }
      </div>
    </div>
  )
}

export default Channel