import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Navbar from './components/Navbar'
import Feed from './components/Feed'
import VideoPage from './components/VideoPage'
import Channel from './components/Channel'
import SearchPage from './components/SearchPage'
import 'flowbite';


const App = () => {
  return (
    <BrowserRouter>
        <div className="yt-main relative min-h-screen dark:bg-black bg-white">
            <Navbar/>
            <Routes>
                <Route path="/" exact element={<Feed/>} />
                <Route path="/video/:id"  element={<VideoPage/>} />
                <Route path="/channel/:id"  element={<Channel/>} />
                <Route path="/search/:search"  element={<SearchPage/>} />
            </Routes>
        </div>
    </BrowserRouter>
  )
}

export default App