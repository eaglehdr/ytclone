import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faMusic, faCode, faSchool, faPodcast, faCamera, faVolleyball, faTv, faDumbbell, faTshirt, faSpa, faHome} from '@fortawesome/free-solid-svg-icons';
// import {faCode} from '@fortawesome/free-solid-svg-icons';
// import {faSchool} from '@fortawesome/free-solid-svg-icons';
// import {faPodcast} from '@fortawesome/free-solid-svg-icons';
// import {faCamera} from '@fortawesome/free-solid-svg-icons';
// import {faVolleyball} from '@fortawesome/free-solid-svg-icons';
// import {faTv} from '@fortawesome/free-solid-svg-icons';
// import {faDumbbell} from '@fortawesome/free-solid-svg-icons';
// import {faTshirt} from '@fortawesome/free-solid-svg-icons';
// import {faSpa} from '@fortawesome/free-solid-svg-icons';


export const logo = 'https://i.ibb.co/s9Qys2j/logo.png';

export const categories = [
  { name: 'New', icon: <FontAwesomeIcon icon={faHome} />, },
  { name: 'JS Mastery', icon: <FontAwesomeIcon icon={faCode} />, },
  { name: 'Coding', icon: <FontAwesomeIcon icon={faCode} />, },
  { name: 'ReactJS', icon: <FontAwesomeIcon icon={faCode} />, },
  { name: 'NextJS', icon: <FontAwesomeIcon icon={faCode} />, },
  { name: 'Music', icon: <FontAwesomeIcon icon={faMusic} /> },
  { name: 'Education', icon: <FontAwesomeIcon icon={faSchool} />, },
  { name: 'Podcast', icon: <FontAwesomeIcon icon={faPodcast} />  , },
  { name: 'Movie', icon: <FontAwesomeIcon icon={faCamera} />, },
  { name: 'Gaming', icon: <FontAwesomeIcon icon={faVolleyball} />, },
  { name: 'Live', icon: <FontAwesomeIcon icon={faTv} />, },
  { name: 'Sport', icon: <FontAwesomeIcon icon={faDumbbell} />, },
  { name: 'Fashion', icon: <FontAwesomeIcon icon={faTshirt} />, },
  { name: 'Beauty', icon: <FontAwesomeIcon icon={faSpa} />, },
  // { name: 'Comedy', icon: <FontAwesomeIcon icon="fas fa-theater-masks" />, },
  // { name: 'Gym', icon: <FontAwesomeIcon icon="fas fa-dumbbell" />, },
  // { name: 'Crypto', icon: <FontAwesomeIcon icon="fas fa-terminal" />, },
];

export const demoThumbnailUrl = 'https://i.ibb.co/G2L2Gwp/API-Course.png';
export const demoChannelUrl = '/channel/UCmXmlB4-HJytD7wek0Uo97A';
export const demoVideoUrl = '/video/GDa8kZLNhJ4';
export const demoChannelTitle = 'JavaScript Mastery';
export const demoVideoTitle = 'Build and Deploy 5 JavaScript & React API Projects in 10 Hours - Full Course | RapidAPI';
export const demoProfilePicture = 'http://dergipark.org.tr/assets/app/images/buddy_sample.png'