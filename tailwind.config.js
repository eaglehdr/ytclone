/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ["./src/**/*.{html,js,jsx}", "./public/index.html", "./node_modules/flowbite/**/*.{js,jsx,ts,tsx}", './node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        ytred: "#fc1503"
      },
    },
  },
  plugins: [
      require('flowbite/plugin'),
      require('@tailwindcss/line-clamp'),
  ]
}
